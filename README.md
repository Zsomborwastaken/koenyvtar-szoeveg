Telefonos Kölcsönzéskezelő Alkalmazás (TKA)

Bevezetés:
=========================
Probléma:
-------------------------
Az iskolákban minden év elején át kell vennie minden diáknak a könyveket. Nagyrésze a diákoknak fizet ezekért e könyvekért, azonban van pár tanuló, akik kedvezményezettek és ingyenesen kapják. Az eddigi évig ezeknek a gyerekeknek a száma pár tucat fő volt. Ezeket az ingyenes tankönyveket vissza kellett adniuk az iskolának az év végén, ezért a könyvtárosoknak fel kellett egy rendszerbe jegyezniük melyik tanuló melyik könyvet kapta meg. Ezt a kevés adat miatt átadásnál egy füzetbe írták be, ahonnan később bevitték egy adatbázisba, ahonnan a tankerület le tudta ellenőrizni ezeket az adatokat. Az idei évtől ezzel szemben közel minden diák ingyenesen juhatott hozzá a tankönyveihez. Ez a 10-20 diákról és nekik a könyveikről közel 800-ra ugrott. A tankönyvek átvételére egy hét van nyár végén. Ennyi adat esetén már nem lehetett fenntartani az eddig használt füzetben vezetős módszert. Mivel erre a problémára nincsen se Magyarországon, se külföldön megfelelő megoldás, nyáron minket kerestek fel az iskola, hogy informatikai támogatást adjunk a probláma megoldására.

Ötlet:
-------------------------
Az ötletünk alapja egy telefonos applikáció, amivel nagy mértékben meggyorsíthatjuk a könyvek regisztrálását egy központi rendszerbe, így a könyvtárosoknak nem kell minden könyvet és tanulót kézzel feljegyezniük. A megoldásunk emellett kiküszöböli az elírást, valamint diáksegítők alkalmazását is könnyen lehetővég teszi. Az applikációban ki lehet választani a tanuló osztályát és ezen belül a tanuló nevét, akihez az adatok fel lesznek jegyezve. Minden könyvben található egy vonalkód, amit bele kellett ragasztaniuk a könyvtárosoknak kiosztás előtt, ezt használjuk fel az adatok egyszerű rögzítéséhez. Egy gombra kattintva előjön a telefon kamerája, amivel könnyedén és gyorsan be lehet olvasni ezt a vonalkódot. Így minden könyvét az adott tanulónak le lehet olvasni, amit a feltöltésig eltárol az alkalmazás. Ha minden könyvnek beolvasásra került a vonalkódja, egy gomb megnyomásával fel lehet tölteni azokat egy központi adatbázisba és jöhet a következő diák. Emelett mi képzeltük el eltárolni a feljegyzett adatokat egy központi adatbázisban, illetve ehhez megvalósítani egy interneten keresztül elérhető kezelőfelületet. Itt nemcsak átnézni lehet a feltöltött adatokat, de itt is fel lehet venni kikölcsönzött könyveket, illetve előre meglévő osztályok, diákok és tankönyvek listáját feltölteni használatra. Emellett a honlapon több felhasználószint is lenne, mind az adminisztrátoroknak, könyvtárosoknak és diáksegítőknek.

Kezdet:
-------------------------
A nyáron érkezett megkeresésre az iskolától elkezdtük készíteni egy kezdetleges verzióját a tervezett alkalmazásnak. Őszre egy applikáció el is készült a nélkülözhetetlen funkciókkal. Így ki tudtuk próbálni mennyire áll helyet az ötletünk. Az applikációval a könyvtárosok könnyen befejezték a rendelkezésre álló egy hét alatt a könyvek regisztrálását, ami sok más iskolának a rövid határidő miatt nagy csúszásokkal, vagy egyáltalán nem sikerült. Ezután találtuk az innovációs pályázatot, ahol szerettük volna befejezni a projectet. Emellett kaptunk megkeresést a tankerülettől is a programunk iránt.

Megvalósítás:
=========================
Telefonos applikáció:
-------------------------
A telefonos applikáció két fő részből épül fel. A főképernyő, ahol a diákokat kiválasztani, könyveket beolvasni és feltölteni, illetve a beállítások képernyő, ahol a serverrel való kapcsolatot lehet létrehozni és felügyelni.

Legelső belépés:
-------------------------
Az alkalmazás legelső indításakor először engedélyezni kell a kamera használatát, illetve az adatok tárolását. Ezután egy "Regisztráció szükséges" üzenet hívja fel a figyelmet az applikáció állapotára. Ezt a beállítások menüpont alatt lehet megtenni a "Kulcs beolvasása" gombra kattintva. Itt egy, a honlap által generált QR-kódot kell beolvasni, ami tartalmazza a szerver címét, ahonnan az adatokat fogja az alkalmazás lekérni, illetve egy kulcsot a biztonság érdekében. Ha ez sikeres volt, és a szerver is elérhető állapotban van, a gomb alatti szövegdobozokban láthatjuk a kapcsolat adatait és állapotát. A visszagombra kattinva a főképernyőn már látni lehet az adatbázisban lévő osztályokat és diákokat.

Használat:
-------------------------
A főképernyő használatával lehet az egyes tanulókhoz könyveket feltölteni. a képernyő legtetején található egy "Oszt" és egy "Diák" felirat, illetve ezek mellett egy-egy legördülő lista. Ezekkel a listákkal lehet először az osztályt, majd a kiválasztott osztály névsorából az adott diák nevét. Ezután a "Beolvasás" gombra kattintva előjön a kamera, amivel le lehet olvasni a könyvön lévő vonalkódot. Ha sikeres a leolvasás, az applikáció visszavált a főképernyőre és egy felugró ablakban kiírja, hogy melyik könyvnek a vonalkódját olvasta le. Ezzel le lehet ellenőrizni, hogy jó volt-e a beolvasás. Ha esetleg nem egyezik a könyvcím, vagy rossz beolvasás esetén először is egy hibaüzenetet dob fel az alkalmazás. A vonalkód ezután a "Beolvasás" gomb alatt lévő szövegdobozban jelenik meg. Abban az esetben itt jelenik meg, ha nem volt jó a beolvasott vonalkód. Ebben az esetleg az ez alatt lévő "Törlés" gombbal lehet a legutóbb vonalkódot kitörölni és újra beolvasni az előzőhöz hasonlóan. Ha a beolvasott vonalkód megfelelő volt, a diák következő könyvének a beolvasása következik ugyanezzel a folyamattal. Abban az esetleg, ha az összes könyv vonalkódja megtalálható a szövegdobozban, a küldés gombra kattintva lehet az adatbázisba feltölteni.

Alkalmazás:
-------------------------
Az alkalmazás Android Studio-ban, Java nyelven lett fejlesztve. Az applikáció a hozzácsatlakoztatott szerverről kéri le az adatokat. Ezek a lekérések PHP nyelven lettek elkészítve. A regisztráció a legelső belépés alatt említett módon történik. Ilyenkor a beállítások képernyőn egy, a honlapon létrehozott QR-kód beolvasásával hozzájutunk a szerver címéhez és egy ideiglenes kulcshoz. Rögtön utána az adott címről a kapott kulccsal lekérjük az alkalmazáshoz tartozó végleges kulcsot és adatokat a kapcsolatról. Ezek az adatok a "Kulcs beolvasása" gomb alatt lévő szövegdobozokban találhatók. Ilyen a "Tankerület" alatt a szerver tankerülethez való tartozása, az "Iskola / Intézmény" alatt az adott intézmény neve, a "Weblap" alatt a meglátogatható weboldal címe, ahonnan elérhetőek a feltöltött az adatok és a "Státusz", a szerverrel való állapot jelzésére. A "Kulcs ujjlenyomata" alatt található egy hash-elt változata a kulcsnak, amit össze lehet hasonlítani a honlaponlévővel, hogy biztosan jó lett-e a beolvasás. Ez legfőképpen biztonsági intézkedés, amellyel nyomon lehet követni melyik alkalmazással milyen adatok és mikor kerültek feltöltésre. Egy kulcs egyetlen alkalmazáshoz tartozhat. Ezután érkezik a szerverre még egy kérés, amivel megkapja az applikáció az osztályok listáját, amit a főképernyőn található legördülő listában lehet megtalálni, visszamenetel esetén. A szerver címét, illetve a kulcsot az alkalmazás eltárolja, így csak az első indításnál kell beolvasni ezt a QR-kódot, ezután már nem kell vele foglalkozni. Ha új szerverhez szeretnénk csatlakozni, vagy kulcsot szeretnénk váltani, ezt egy újabb kód beolvasásával könnyedén meg lehet tenni.
Ezután, illetve minden ezutáni indítás esetén, a főképernyő megnyitása esetén az osztályok és a diákok legördülő lista ki lesz töltve. A diákok listában mindig az aktuálisan kiválasztott osztály névsora látható, ami új osztály kiválasztása esetén a szervertől való adatok lekérése után változik. A "Küldés" gomb megnyomása esetén lévő adatfeltöltés is egy ilyen kérés utána történik meg. Természetesen minden kérésben szerepel az alkalmazáshoz tartozó kulcs azonosítás szempontjából.
A reszponzivitás széles körűen van megoldva, kisképernyőjű telefontól akár táblagép használata esetére is alkalmas.
Az alkalmazásban előforduló hibák és üzenetek kétféleképpen jelenhetnek meg. A kevésbé jelentőségteljesek egy, a képernyő alján pár másodpercig megjelenő üzenetként. Ilyen például, hogy hány könyv feltöltése történt meg egy adott diákhoz, ha 0 beolvasása esetén történne adatküldés, illetve üres szövegdobozből való törlés esetén. A másik esetben, hogyha valamilyen hiba, vagy fontos üzenet érkezik, egy felugró ablak jön fel a képernyő közepére, az üzenettel, amit kattintással lehet leláttamozni. Ilyen lehet, hogyha nem sikerült a könyveket feltölteni, ha valamilyen hiba lépett fel a szerverrel való kommunikálás közben, vagy egy könyv vonalkódjának leolvasása után. Abban az esetben, ha egy hiba lépett fel, a felhasználó értesítést kap egy felugró ablak formályában, emellett ezt a hibát elmenti az alkalmazás egy fájlba. Ehhez van szükség az adatok tárolásának engedélyezésére az első indításnál. A hibáknak a pontos időpontja is elmentésre kerül. Az üzenet nem mindig azonos a pontos hibával, ilyen lehet például a "Szerveroldali hiba" üzenet, aminek viszont a pontos leírása mentődik el.



Emberek szemszögéből(diák, könyvtáros, admin)

Adatbázis:
pass

Kezelőfelület:
pass

Nyilt forráskód:
pass

Befejezés:
pass
